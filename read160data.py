# coding:utf8

import os
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from datadownload import BASEDIR,DATADIR,ELE,MON
from stations import *


SEASON = {
    'DJF': ['12', '01', '02'],
    'MAM': ['03', '04', '05'],
    'JJA': ['06', '07', '08'],
    'SON': ['09', '10', '11'],
    'MJJA': ['05', '06', '07', '08'],
    'MJJAS': ['05', '06', '07', '08', '09'],
    'MJJASO': ['05', '06', '07', '08', '09', '10'],
    'SO': ['09', '10'],
    'YEAR': [str(i).zfill(2) for i in range(1,13)],
}


SUM_MEAN = {
    't': np.mean,
    'r': np.sum,
}


# 读取单个文件，单月
def read160data(ele,mon):
    datafile = os.path.join(DATADIR,'%s16%s' % (ele,mon))
    data160 = np.loadtxt(datafile)
    nyears = int(data160.size / 160)
    data160 = np.reshape(data160.ravel(),(nyears,160))
    if ele == 't':
        data160 = np.round(data160*0.1,1)
    data160 = np.ndarray.tolist(data160)
    return data160


# 按站点名称读取文件，单月
def getData_by_name(ele,mon,name):
    data160 = read160data(ele,mon)
    index = getIndex_by_name(name)
    data = [i[index] for i in data160]
    year = list(range(1951,1951+len(data)))
    return year,data


# 按若干站名读取文件，单月
def getData_by_names(ele,mon,names):
    data160 = read160data(ele,mon)
    indexes = [getIndex_by_name(name) for name in names]
    datas = [[i[index] for i in data160] for index in indexes]
    years = [list(range(1951,1951+len(data))) for data in datas]
    return years,datas


# 按站点名读取季节或年值，气温平均，降水累积
def getData_by_name_season(ele,season,name):
    sum_mean = SUM_MEAN.get(ele)
    season = SEASON.get(season)
    years,datas = [list(range(len(season))) for i in (0,1)]
    for i in years:
        mon = season[i]
        years[i],datas[i] = getData_by_name(ele,mon,name)
    leastnyears = min([len(i) for i in years])
    if season != 'DJF':
        year = list(range(1951,1951+leastnyears))
        datas = [i[:leastnyears] for i in datas]
    else:
        year = list(range(1951,1951+leastnyears-1))
        datas = [i[:leastnyears] for i in datas]
        datas[0] = datas[0][1:]
        datas[1] = datas[1][:-1]
        datas[2] = datas[2][:-1]
    data = sum_mean(datas,axis=0)
    data = [round(i,1) for i in data]
    return year,data


# 读取若干站，季节或年值，气温平均，降水累积
def getData_by_names_season(ele,season,names):
    years,datas = [list(range(len(names))) for i in (0,1)]
    for i in years:
        name = names[i]
        years[i],datas[i] = getData_by_name_season(ele,season,name)
    return years,datas


# 计算单站，单月距平，距平百分率
def getAbnorm_by_name(ele,mon,name):
    year,data = getData_by_name(ele,mon,name)
    norm = np.mean(data[year.index(1981):year.index(2011)])
    abnorm = [i-norm for i in data]
    abnorm_percent = [i*100.0/norm for i in abnorm]
    norm = round(norm,1)
    abnorm = np.round(abnorm,1)
    abnorm_percent = np.round(abnorm_percent)
    return year,norm,abnorm,abnorm_percent


# 计算若干站，单月距平，距平百分率
def getAbnorm_by_names(ele,mon,names):
    years,norms,abnorms,abnorm_percents = [list(range(len(names))) for i in range(4)]
    for i in years:
        name = names[i]
        years[i],norms[i],abnorms[i],abnorm_percents[i] = getAbnorm_by_name(ele,mon,name)
    return years,norms,abnorms,abnorm_percents


# 计算单站，季节或年距平，距平百分率
def getAbnorm_by_name_season(ele,season,name):
    year,data = getData_by_name_season(ele,season,name)
    norm = np.mean(data[year.index(1981):year.index(2011)])
    abnorm = [i-norm for i in data]
    abnorm_percent = [i*100.0/norm for i in abnorm]
    norm = round(norm,1)
    abnorm = np.round(abnorm,1)
    abnorm_percent = np.round(abnorm_percent)
    return year,norm,abnorm,abnorm_percent


# 计算若干，站季节或年距平，距平百分率
def getAbnorm_by_names_season(ele,season,names):
    years,norms,abnorms,abnorm_percents = [list(range(len(names))) for i in range(4)]
    for i in years:
        name = names[i]
        years[i],norms[i],abnorms[i],abnorm_percents[i] = getAbnorm_by_name_season(ele,season,name)
    return years,norms,abnorms,abnorm_percents


# 计算单站，单月，若干年分合成距平，距平百分率
def meanAbnorm_mon_by_name_years(ele,mon,name,years):
    year,norm,abnorm,abnorm_percent = getAbnorm_by_name(ele,mon,name)
    abnorms,abnorm_percents = [list(range(len(years))) for i in (0,1)]
    for i in abnorms:
        abnorms[i],abnorm_percents[i] = [a[year.index(years[i])] for a in (abnorm,abnorm_percent)]
    abnorm,abnorm_percent = [round(np.mean(i),1) for i in (abnorms,abnorm_percents)]
    return abnorm,abnorm_percent


# 计算若干站，单月，若干年分合成距平，距平百分率
def meanAbnorm_mon_by_names_years(ele,mon,names,years):
    abnorms,abnorm_percents = [list(range(len(names))) for i in (0,1)]
    for i in abnorms:
        name = names[i]
        abnorms[i],abnorm_percents[i] = meanAbnorm_mon_by_name_years(ele,mon,name,years)
    return abnorms,abnorm_percents


# 计算单站，季节或年，若干年分合成距平，距平百分率
def meanAbnorm_season_by_name_years(ele,season,name,years):
    year,norm,abnorm,abnorm_percent = getAbnorm_by_name_season(ele,season,name)
    abnorms,abnorm_percents = [list(range(len(years))) for i in (0,1)]
    for i in abnorms:
        abnorms[i],abnorm_percents[i] = [a[year.index(years[i])] for a in (abnorm,abnorm_percent)]
    abnorm,abnorm_percent = [round(np.mean(i),1) for i in (abnorms,abnorm_percents)]
    return abnorm,abnorm_percent


# 计算若干站，季节或年，若干年分合成距平，距平百分率
def meanAbnorm_season_by_names_years(ele,season,names,years):
    abnorms,abnorm_percents = [list(range(len(names))) for i in (0,1)]
    for i in abnorms:
        name = names[i]
        abnorms[i],abnorm_percents[i] = meanAbnorm_season_by_name_years(ele,season,name,years)
    return abnorms,abnorm_percents


if __name__ == '__main__':
    year,data = getData_by_name('t', '12', u'岷县')
    plt.plot(year, data)
    plt.show()

    # years,datas = getData_by_names('r','09',[u'北京',u'天津',u'济南'])
    # print years,datas

    # year,data = getData_by_name_season('t','MAM',u'北京')
    # print year,data

    # year,norm,abnorm,abnorm_percent = getAbnorm_by_name('t','05',u'上海')
    # print year,norm,abnorm,abnorm_percent
    # plt.plot(year,abnorm)
    # plt.savefig('sample.png')

    # years,norms,abnorms,abnorm_percents = getAbnorm_by_names('r','05',[u'上海',u'济南',u'北京',u'天津'])
    # print years,norms,abnorms,abnorm_percents

    # year,norm,abnorm,abnorm_percent = getAbnorm_by_name_season('r','JJA',u'上海')
    # print year,norm,abnorm,abnorm_percent

    # years,norms,abnorms,abnorm_percents = getAbnorm_by_names_season('r','JJA',[u'上海',u'济南',u'北京',u'天津'])
    # print years,norms,abnorms,abnorm_percents

    # abnorm,abnorm_percent = meanAbnorm_mon_by_name_years('r','07',u'上海',[1951,1964,2000,2014])
    # print abnorm,abnorm_percent

    # abnorms,abnorm_percents = meanAbnorm_season_by_names_years('r','JJA',[u'上海',u'济南',u'北京',u'天津'],[1951,1964,2000,2014])
    # print abnorms,abnorm_percents

    # ele = 'r'
    # season = 'MJJAS'
    # names = NAME
    # years = [1952,1964,1971,1982,1999,2014]
    # abnorms,abnorm_percents = meanAbnorm_season_by_names_years(ele,season,names,years)
    # print abnorms,abnorm_percents

    print len(read160data('r', '03'))
    print len(read160data('r', '04'))
    print len(read160data('r', '05'))
    print len(read160data('r', '06'))


