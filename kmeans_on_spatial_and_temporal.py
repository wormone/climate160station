#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2016-08-09 10:25:12
# @Author  : wormone (renyuuu@gmail.com)

from __future__ import unicode_literals

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans, SpectralClustering
from sklearn.metrics import silhouette_samples, silhouette_score, pairwise

import read160data as R160s
import download_climateIndex as CI
from stationSeries_corr_climateIndex import read_to_grade


ele, period = 'r', 'JJA'
grade_method, isAbnorm, from_year, to_year, isPrint = 'fixed', True, 1951, 2015, False

years = range(from_year, to_year+1)

sites = R160s.NAME
ys, grades = [list(range(160)) for _ in range(2)]
for _ in ys:
    y_grade = read_to_grade(ele, period, sites[_], 
        grade_method=grade_method, isAbnorm=isAbnorm, 
        from_year=from_year, to_year=to_year, isPrint=isPrint)
    y, grade = [y_grade.get(k) for k in ['y', 'grade']]
    ys[_], grades[_] = list(y), list(grade['real'])
ys, grades = [np.array(_) for _ in [ys, grades]]

ys_spatial, grades_spatial = ys, grades
ys_temporal, grades_temporal = ys.T, grades.T

# np.savetxt('ys_temporal.csv', ys_temporal, fmt=b'%1.0f', delimiter=',')
# np.savetxt('ys_spatial.csv', ys_spatial, fmt=b'%1.0f', delimiter=',')
# np.savetxt('grades_temporal.csv', grades_temporal, fmt=b'%1.0f', delimiter=',')
# np.savetxt('grades_spatial.csv', grades_spatial, fmt=b'%1.0f', delimiter=',')

X = ys_temporal
N = range(2, 64)
silhouette_avgs = list(range(len(N)))
D = pairwise.pairwise_distances(X, metric='correlation')
print X.shape, D.shape
for _ in silhouette_avgs:
    clusterer = SpectralClustering(n_clusters=N[_], affinity='precomputed', random_state=10)
    cluster_labels = clusterer.fit_predict(D)
    # silhouette_avgs[_] = silhouette_score(X, cluster_labels, metric='correlation') # replaced by the following line:
    silhouette_avgs[_] = silhouette_score(D, cluster_labels, metric='precomputed')
plt.subplot(2, 1, 1)
plt.plot(N, silhouette_avgs)
plt.xlabel('Number of Clusters')
plt.ylabel('Mean Silhouette Value')
plt.subplot(2, 1, 2)
plt.plot(years, X)
plt.xlabel('Years')
plt.ylabel('Data')
plt.show()