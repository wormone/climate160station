#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from stationSeries_corr_climateIndex import *


if __name__ == '__main__':
    ele = 't'
    period = '10'
    
    kw = dict(
        ele = ele,
        period = period,
        from_last_year_month = 4,
        to_this_year_month = -2,
        from_year = 1976,
        to_year = 2017,
        grade_method = 'fixed_temperature')

    # fcst_160_stations_parallel(clf='forest', **kw)

    sites = ['岷县']
    summary, fcsted = [list(range(len(sites))) for _ in range(2)]
    for _ in range(len(sites)):
        summary[_], fcsted[_] = once(sites[_], clf='forest', **kw)
    summary, fcsted = [pd.concat(_) for _ in [summary, fcsted]]
    summary.to_csv('minxian_{}_{}_summary.csv'.format(ele, period), encoding='utf8')
    fcsted.to_csv('minxian_{}_{}_fcsted.csv'.format(ele, period), encoding='utf8')
