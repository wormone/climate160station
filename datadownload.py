# coding:utf8

import os
import sys

from download_climateIndex import wget


"""URL

http://cmdp.ncc-cma.net/cn/download.htm
"""


BASEDIR = os.path.abspath(os.path.dirname(__file__))
DATADIR = os.path.join(BASEDIR,'data')
BASEURL = 'http://cmdp.ncc-cma.net/upload/upload8/'
ELE = ['t', 'r']
MON = [str(i).zfill(2) for i in range(1, 13)]


def main():
    for ele in ELE:
        for mon in MON:
            fn = '{}16{}'.format(ele, mon)
            url = BASEURL + fn
            save_to_fn = os.path.join(DATADIR, fn)
            try:
                wget(url, save_to_fn)
            except:
                print('Fail for', save_to_fn)


if __name__ == '__main__':
    main()
