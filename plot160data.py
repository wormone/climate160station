# coding:utf8

from read160data import *

import random
from datetime import datetime
from mpl_toolkits.basemap import Basemap
from scipy.interpolate import griddata

from province import province as PROVINCE
from isinchina import points_in_china
from notinchina import notinchina as NOTINCHINA


MYFONTFILE = os.path.join(BASEDIR,'static','msyh.ttc')
MYFONT = matplotlib.font_manager.FontProperties(fname=MYFONTFILE,size=8)

PNG_OUT_DIR = os.path.join(BASEDIR,'static','images')

M = Basemap(width=5500000,height=4500000,
    resolution='l',projection='aea',
    lat_1=27,lat_2=45,lon_0=105,lat_0=36)

POINTS = [(X[i],Y[i]) for i in range(len(X))]

delta_x, delta_y = 141, 111

X0,Y0 = np.mgrid[70:140:delta_x*1j, 0:55:delta_y*1j]

X00,Y00 = [i.ravel() for i in (X0,Y0)]

# POINTS0 = [(X00[i],Y00[i]) for i in range(len(X00))]
# inchina = points_in_china(POINTS0)
# NOTINCHINA = [not b for b in inchina]
# with open('notinchina.py','w') as maskfile:
#    maskfile.write('notinchina=%s' % str(NOTINCHINA))

X000,Y000 = M(X00,Y00)
X000,Y000 = [np.reshape(i,(delta_x,delta_y)) for i in (X000,Y000)]


def plot160data(ele,abnorms):
    # interpolate
    points, abnorms = POINTS, list(abnorms)
    points += [(0,0),(0,90),(180,0),(180,90)]
    abnorms += [0,0,0,0]
    Z0 = griddata(np.array(points),np.array(abnorms),(X00,Y00),method='cubic')
    Z0 = np.ma.array(Z0, mask=NOTINCHINA)
    Z0 = np.reshape(Z0,(delta_x,delta_y))

    CMAP = {'t':plt.cm.rainbow, 'r':plt.cm.Spectral}
    cmap = CMAP.get(ele)

    M.contourf(X000,Y000,Z0,cmap=cmap)

    # plot province boundaries
    plt.hold(True)
    for p in PROVINCE:
        px,py = [[i[j] for i in p] for j in (0,1)]
        px,py = M(px,py)
        M.plot(px,py,'k',linewidth=0.3)

    # plot 160 stations, X and Y are imported from stations.py
    Xm,Ym = M(X,Y)
    plt.plot(Xm,Ym,'k.',ms=4)
    # plt.plot(Xm,Ym,'k.',ms=5,alpha=0.618)

    # plt.axis('off')
    # plt.rc('text', usetex=True)
    plt.rc('font', family='sans-serif', size=9)
    cbar = plt.colorbar(orientation='vertical',shrink=0.4)
    # ticklabels = cbar.ax.yaxis.get_ticklabels()
    # print ticklabels

    # random output file name
    pngfilename = datetime.strftime(datetime.now(),'%Y%m%d%H%M%S') + \
        'R' + str(random.randint(1000,2000)) + '.png'
    pngfilename_full = os.path.join(PNG_OUT_DIR,pngfilename)
    plt.savefig(pngfilename_full,bbox_inches='tight',pad_inches=0.3,dpi=200)

    plt.clf()

    return pngfilename


def plotline(ele,years,datas):
    year = years[0]
    data = np.round(np.mean(datas,axis=0),1)

    if ele == 't':
        plt.plot(year,data,'yo-')
    if ele == 'r':
        plt.plot(year,data,'bo-')
    ax = plt.gca()
    ax.grid('on')

    # random output file name
    pngfilename = datetime.strftime(datetime.now(),'%Y%m%d%H%M%S') + \
        'R' + str(random.randint(1000,2000)) + '.png'
    pngfilename_full = os.path.join(PNG_OUT_DIR,pngfilename)
    plt.savefig(pngfilename_full,bbox_inches='tight',pad_inches=0.3,dpi=200)

    plt.clf()

    return pngfilename


if __name__ == '__main__':
    names = NAME
    ele = 't'
    season = 'DJF'
    years = [1952,1964,1999,2014]
    abnorms,abnorm_persents = meanAbnorm_season_by_names_years(ele,season,names,years)
    plot160data(ele,abnorms)

    years,norms,abnorms,abnorm_persents = getAbnorm_by_names_season('t','DJF',NAME)
    pngfilename = plotline('t',years,abnorms)
