
// 切换曲线图和合成图
$('label[for="chartmap-radio-c"]').on('tap',function(){
  $('label[for="chartmap-radio-c"]').trigger('click');
});
$('label[for="chartmap-radio-d"]').on('tap',function(){
  $('label[for="chartmap-radio-d"]').trigger('click');
});
$('label[for="chartmap-radio-c"]').on('click',function(){
  $('#div-yearsmean').hide();
  $('#div-stationsmean').show();
});
$('label[for="chartmap-radio-d"]').on('click',function(){
  $('#div-yearsmean').show();
  $('#div-stationsmean').hide();
});
$('input#reset').bind('click',function(){
  $('#div-yearsmean').hide();
  $('#div-stationsmean').show();
  $('#reset').html(''); // 重置清空结果
});

// 设定copyright年份
var now = new Date();
$('#theyear').html(now.getFullYear());

// 提示信息
function thealert(title,body){
  $('h3#the_alert_title').text(title);
  $('p#the_alert_body').text(body);
  $('#alert').trigger('click');
}

// show and hide Loader
function showLoader(){
  $.mobile.loading( "show", {
    text: '加载中……',
    textVisible: true,
    theme: 'b',
    textonly: false,
    html: ''
  });
}
function successsLoaded(){
  $.mobile.loading( "show", {
    text: '加载完成',
    textVisible: true,
    theme: 'b',
    textonly: true,
    html: ''
  });
}
function hideLoader(){
  $.mobile.loading( "hide");
}

// CSRF
var csrftoken = $('meta[name=csrf-token]').attr('content');
$.ajaxSetup({
    type:'POST',
    dataType: 'json',
    beforeSend: function(xhr, settings) {
        if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken)
        }
    }
});

// Ajax
$('#submit').click(function(){
  var ele = $('input[name="ele-radio"]:checked').val();
  var output = $('input[name="output-radio"]:checked').val();
  var period = $('input#period').val();
  var chartmap = $('input[name="chartmap-radio"]:checked').val();
  var yearsmean = $('input#yearsmean').val();
  var stationsmean = $('input#stationsmean').val();
  var query = {
    ele:ele,
    output:output,
    period:period,
    chartmap:chartmap,
    yearsmean:yearsmean,
    stationsmean:stationsmean,
  };
  // console.log(query);
  if (chartmap=='map'&&output=='raw') {
    thealert('提示','分布图请选择距平或距平百分率！'); return false;
  }
  else if (yearsmean||stationsmean) {
    setTimeout('showLoader()',100);
    $.post($SCRIPT_ROOT + '/_do', query, function(json){
      if (json.Error||$.isEmptyObject(json)) {
        thealert('消息','抱歉，出错了:-(');
      } else if (json.hasOwnProperty('url_pngfile')) {
        $('#result').html('<img src="' + json.url_pngfile + '" style="width:100%;">');
      };
    });
  } else {
    thealert('错误','年份或站点未设定！'); return false;
  };
});

// delay hiding loader
$(document).ajaxStop(function(){
  successsLoaded();
  setTimeout('hideLoader()', 2000);
});
