# coding:utf8
from __future__ import unicode_literals

import numpy as np
from collections import namedtuple

from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.utils.np_utils import to_categorical
from keras.optimizers import SGD

from stationSeries_corr_climateIndex import *


def get_one_site(ele='r', period='JJA', site='杭州',
                 from_last_year_month = 7, to_this_year_month = 4,
                 from_year = 1952, to_year = 2010, end_year = 2015,
                 grade_method='auto', isAbnorm=True,
                 sig=0.1, **kw):

    years_train = range(from_year, to_year + 1)
    years_fcst = range(to_year + 1, end_year + 1)

    y_grade = read_to_grade(ele, period, site, 
        grade_method=grade_method, isAbnorm=isAbnorm, from_year=from_year, to_year=end_year)
    y, grade, norm = [y_grade[_] for _ in ['y', 'grade', 'norm']]

    y_train, grade_train = [_.ix[from_year:to_year] for _ in [y, grade]]
    y_fcst, grade_fcst = [_.ix[to_year+1:end_year] for _ in [y, grade]]

    divider = y_grade['theGrades']['divider']
    Col_Month_Name_Lag = filter_factors(
        y_train, from_last_year_month, to_this_year_month, sig=sig, **kw) # y_train or y

    predictors_train, predictors_fcst = [
        get_predictors(_, Col_Month_Name_Lag)['predictors'] 
        for _ in [years_train, years_fcst]]

    y_train, grade_train, y_fcst, grade_fcst = [
        np.array(_) for _ in [y_train, grade_train, y_fcst, grade_fcst]]

    Output = namedtuple('Output', 
        ['y_train', 'grade_train', 'y_fcst', 'grade_fcst', 
         'predictors_train', 'predictors_fcst', 'divider', 'norm']
    )
    data = Output(y_train, grade_train, y_fcst, grade_fcst, 
                  predictors_train, predictors_fcst, divider, norm)
    return data


def keras_fcst(data):
    """
        INPUTS:
            data: as returned by get_one_site()
    """
    predictors_train = data.predictors_train
    grade_train = data.grade_train.ravel()

    predictors_fcst = data.predictors_fcst
    grade_tobe_fcst = data.grade_fcst

    input_dim = predictors_train.shape[1]
    nb_epoch = 1000
    batch_size = 32
    activation = 'softmax'
    optimizer = SGD(lr=0.01, decay=1e-6, momentum=0.618, nesterov=True)
    loss = 'categorical_crossentropy'
    num_labels = len(set(grade_train))
    grade_train = to_categorical(grade_train, num_labels)

    model = Sequential()
    model.add(Dense(num_labels, input_dim=input_dim, activation=activation))
    model.compile(optimizer=optimizer,
                  loss=loss,
                  metrics=['accuracy'])
    model.fit(predictors_train, grade_train, nb_epoch=nb_epoch, batch_size=batch_size)
    grade_fcst = model.predict_classes(predictors_fcst, batch_size=batch_size)
    proba = model.predict_proba(predictors_fcst, batch_size=batch_size)
    df = pd.DataFrame(dict(
        grade_tobe_fcst=grade_tobe_fcst.ravel(),
        grade_fcst=grade_fcst.ravel()))
    return df


def KNeighbors_fcst(data):
    predictors_train = data.predictors_train
    grade_train = data.grade_train.ravel()

    predictors_fcst = data.predictors_fcst
    grade_tobe_fcst = data.grade_fcst

    from sklearn.neighbors import KNeighborsClassifier
    n_neighbors = 5
    weights = 'uniform' # 'distance'
    clf = KNeighborsClassifier(n_neighbors, weights=weights)
    clf.fit(predictors_train, grade_train)
    grade_fcst = clf.predict(predictors_fcst)

    df = pd.DataFrame(dict(
        grade_tobe_fcst=grade_tobe_fcst.ravel(),
        grade_fcst=grade_fcst.ravel()))
    return df


def svm_fcst(data):
    predictors_train = data.predictors_train
    grade_train = data.grade_train.ravel()

    predictors_fcst = data.predictors_fcst
    grade_tobe_fcst = data.grade_fcst

    from sklearn import svm
    # clf = svm.SVC(decision_function_shape='ovo')
    clf = svm.LinearSVC()
    clf.fit(predictors_train, grade_train)
    grade_fcst = clf.predict(predictors_fcst)

    df = pd.DataFrame(dict(
        grade_tobe_fcst=grade_tobe_fcst.ravel(),
        grade_fcst=grade_fcst.ravel()))
    return df



def test():
    kw = dict(
        site = '北京',
        ele = 't',
        period = '12',
        from_last_year_month = 2,
        to_this_year_month = 9,
        from_year = 1976,
        to_year = 2010,
        end_year = 2015,
        grade_method = 'fixed_temperature',
        isAbnorm = True,
        sig = 0.005)
    data = get_one_site(**kw)

    df = keras_fcst(data)
    # df = KNeighbors_fcst(data)
    # df = svm_fcst(data)
    print df


def test_iris():
    import numpy as np
    from sklearn.model_selection import train_test_split
    from sklearn import datasets
    from sklearn import svm

    iris = datasets.load_iris()
    print iris.data.shape, iris.target.shape

    X_train, X_test, y_train, y_test = train_test_split(
        iris.data, iris.target, test_size=0.4, random_state=0)

    print X_train.shape, y_train.shape
    print X_test.shape, y_test.shape

    clf = svm.SVC(kernel='linear', C=1).fit(X_train, y_train)
    print clf.score(X_test, y_test)


    input_dim = X_train.shape[1]
    nb_epoch = 1000
    batch_size = 32
    activation = 'softmax' # 'linear'
    optimizer = SGD(lr=0.01, decay=1e-6, momentum=0.618, nesterov=True)
    loss = 'categorical_crossentropy' # 'mse'
    num_labels = len(set(y_train))
    if loss == 'categorical_crossentropy':
        y_train = to_categorical(y_train, num_labels)

    model = Sequential()
    model.add(Dense(num_labels if activation=='softmax' else 1, input_dim=input_dim, activation=activation))
    model.compile(optimizer=optimizer,
                  loss=loss,
                  metrics=['accuracy'])
    model.fit(X_train, y_train, nb_epoch=nb_epoch, batch_size=batch_size)
    y_fcsted = model.predict_classes(X_test, batch_size=batch_size)
    proba = model.predict_proba(X_test, batch_size=batch_size)
    df = pd.DataFrame(dict(grade_tobe_fcst=y_test.ravel(), grade_fcst=y_fcsted.ravel()))
    print df


if __name__ == '__main__':
    # test()
    test_iris()
