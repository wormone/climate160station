#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import matplotlib as mpl
from matplotlib.colors import LinearSegmentedColormap

# mpl.rcParams['text.usetex'] = True
# mpl.rcParams['text.latex.unicode'] = True

from stationSeries_corr_climateIndex import *

grade_label = ['异常偏多', '偏多', '正常略偏多', '正常略偏少', '偏少', '异常偏少']


site = '岷县'

ele = 'r'
period = '04'
isAbnorm = True
from_year = 1952
to_year = 2017
grade_method = 'fixed'

real = [4, 5, 4, 2, 5]  # 2013 -- 2017
fcst_grade = [5, 4, 4, 1, 5]  # 2013 -- 2017

fcst_next_year = 4  # 2018

prob_1 = [0.1, 0.06, 0.06, 0.25, 0.13, 0.4]    # 2017
prob_2 = [0.04, 0.34, 0.23, 0.11, 0.25, 0.03]  # 2016
prob_3 = [0.06, 0.04, 0.16, 0.23, 0.31, 0.2]   # 2015
prob_4 = [0.2, 0.04, 0.17, 0.09, 0.43, 0.07]   # 2014
prob_5 = [0.12, 0.09, 0.07, 0.19, 0.19, 0.34]  # 2013

prob_0 = [0.13, 0.02, 0.14, 0.16, 0.4, 0.15]   # 2018


prob_all = [prob_5, prob_4, prob_3, prob_2, prob_1, prob_0]
year_prob = range(to_year-4, to_year+2)


year_origin = 1950


y_grade = read_to_grade(ele, period, site, 
    grade_method=grade_method, isAbnorm=isAbnorm, from_year=from_year, to_year=to_year)
y, grade, norm = [y_grade[_] for _ in ['y', 'grade', 'norm']]
divider = y_grade['theGrades']['divider']

y_raw = (np.array(y) * 0.01 + 1) * norm
dv = (np.array(divider) * 0.01 + 1) * norm

y_min = 0
y_max = 120


CMAP = plt.cm.Blues
NORM = mpl.colors.Normalize(vmin=0, vmax=1)

MYFONTFILE = os.path.join('.', 'msyh.ttc')
MYFONT, MYFONT_CITY = [
    mpl.font_manager.FontProperties(fname=MYFONTFILE, size=size) for size in (13, 17)
]

pp = np.array(prob_all).T[::-1, :]


plt.rc('font', family='sans-serif', size=13)

plt.imshow(pp, interpolation='nearest', cmap=CMAP, norm=NORM)
cbar = plt.colorbar(orientation='vertical', shrink=1.0)
cbar.set_label('概 率 估 计', fontproperties=MYFONT)

plt.plot(range(5), 5 - np.array(real), 'ko', ms=5)
plt.plot(range(5), 5 - np.array(fcst_grade), 'ko', mfc='none', ms=9)

plt.plot(5, 5 - fcst_next_year, 'o', mfc='none', markeredgecolor='r', ms=9)

plt.xticks(range(6), year_prob)
plt.yticks(range(6), grade_label, fontproperties=MYFONT)
plt.axis('tight')

plt.title('{} {} {} 趋势预测'.format(site, '4月', {'r': '降水', 't': '气温'}.get(ele)), fontproperties=MYFONT_CITY, loc='left')
plt.title('● 实际  ○ 预测', fontproperties=MYFONT, loc='right')

plt.show()
