#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2016-07-19 15:11:49
# @Author  : wormone (renyuuu@gmail.com)

from __future__ import unicode_literals

import os
import re
import urllib2
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from collections import OrderedDict


BASE_DIR = os.path.dirname(os.path.abspath(__file__))

BASE_URL = 'http://cmdp.ncc-cma.net/download/Monitoring/Index/'
DATA_URLS = {
    'Atm': 'M_Atm_Nc.txt',
    'Oce': 'M_Oce_Er.txt',
    'Ext': 'M_Ext.txt',
}


Atm = OrderedDict([
    (1, '北半球副高面积指数 (Northern Hemisphere Subtropical High Area Index)'),
    (2, '北非副高面积指数 (North African Subtropical High Area Index)'),
    (3, '北非-大西洋-北美副高面积指数 (North African-North Atlantic-North American Subtropical High Area Index)'),
    (4, '印度副高面积指数 (Indian Subtropical High Area Index)'),
    (5, '西太平洋副高面积指数 (Western Pacific Subtropical High Area Index)'),
    (6, '东太平洋副高面积指数 (Eastern Pacific Subtropical High Area Index)'),
    (7, '北美副高面积指数 (North American Subtropical High Area Index)'),
    (8, '北大西洋副高面积指数 (Atlantic Subtropical High Area Index)'),
    (9, '南海副高面积指数 (South China Sea Subtropical High Area Index)'),
    (10, '北美大西洋副高面积指数 (North American-Atlantic Subtropical High Area Index)'),
    (11, '北太平洋副高面积指数 (Pacific Subtropical High Area Index)'),
    (12, '北半球副高强度指数 (Northern Hemisphere Subtropical High Intensity Index)'),
    (13, '北非副高强度指数 (North African Subtropical High Intensity Index)'),
    (14, '北非-北大西洋-北美副高强度指数 (North African-North Atlantic-North American Subtropical High Intensity Index)'),
    (15, '印度副高强度指数 (Indian Subtropical High Intensity Index)'),
    (16, '西太平洋副高强度指数 (Western Pacific Subtropical High Intensity Index)'),
    (17, '东太平洋副高强度指数 (Eastern Pacific Subtropical High Intensity Index)'),
    (18, '北美副高强度指数 (North American Subtropical High Intensity Index)'),
    (19, '北大西洋副高强度指数 (North Atlantic Subtropical High Intensity Index)'),
    (20, '南海副高强度指数 (South China Sea Subtropical High Intensity Index)'),
    (21, '北美-北大西洋副高强度指数 (North American-North Atlantic Subtropical High Intensity Index)'),
    (22, '太平洋副高强度指数 (Pacific Subtropical High Intensity Index)'),
    (23, '北半球副高脊线位置指数 (Northern Hemisphere Subtropical High Ridge Position Index)'),
    (24, '北非副高脊线位置指数 (North African Subtropical High Ridge Position Index)'),
    (25, '北非-北大西洋-北美副高脊线位置指数 (North African-North Atlantic-North American Subtropical High Ridge Position Index)'),
    (26, '印度副高脊线位置指数 （Indian Subtropical High Ridge Position Index)'),
    (27, '西太平洋副高脊线位置指数 （Western Pacific Subtropical High Ridge Position Index)'),
    (28, '东太平洋副高脊线位置指数 (Eastern Pacific Subtropical High Ridge Position Index)'),
    (29, '北美副高脊线位置指数 (North American Subtropical High Ridge Position Index)'),
    (30, '大西洋副高脊线位置指数 (Atlantic Sub Tropical High Ridge Position Index)'),
    (31, '南海副高脊线位置指数 (South China Sea Subtropical High Ridge Position Index)'),
    (32, '北美-北大西洋副高脊线位置指数 (North American-North Atlantic Subtropical High Ridge Position Index)'),
    (33, '北太平洋副高脊线位置指数 (Pacific Subtropical High Ridge Position Index)'),
    (34, '北半球副高北界位置指数 (Northern Hemisphere Subtropical High Northern Boundary Position Index)'),
    (35, '北非副高北界位置指数 (North African Subtropical High Northern Boundary Position Index)'),
    (36, '北非-北大西洋-北美副高北界位置指数 (North African-North Atlantic-North American Subtropical High Northern Boundary Position Index)'),
    (37, '印度副高北界位置指数 ( Indian Subtropical High Northern Boundary Position Index)'),
    (38, '西太平洋副高北界位置指数 (Western Pacific Subtropical High Northern Boundary Position Index)'),
    (39, '东太平洋副高北界位置指数 (Eastern Pacific Subtropical High Northern Boundary Position Index)'),
    (40, '北美副高北界位置指数 (North American Subtropical High Northern Boundary Position Index)'),
    (41, '北大西洋副高北界位置指数 (Atlantic Subtropical High Northern Boundary Position Index)'),
    (42, '南海副高北界位置指数 (South China Sea Subtropical High Northern Boundary Position Index)'),
    (43, '北美-北大西洋副高北界位置指数 (North American-Atlantic Subtropical High Northern Boundary Position Index)'),
    (44, '北太平洋副高北界位置指数(Pacific Subtropical High Northern Boundary Position Index)'),
    (45, '西太平洋副高西伸脊点指数（Western Pacific Sub Tropical High Western Ridge Point Index)'),
    (46, '亚洲区极涡面积指数 (Asia Polar Vortex Area Index)'),
    (47, '太平洋区极涡面积指数 (Pacific Polar Vortex Area Index)'),
    (48, '北美区极涡面积指数 (North American Polar Vortex Area Index)'),
    (49, '大西洋欧洲区极涡面积指数 (Atlantic-European Polar Vortex Area Index)'),
    (50, '北半球极涡面积指数 (Northern Hemisphere Polar Vortex Area Index)'),
    (51, '亚洲区极涡强度指数 (Asia Polar Vortex Intensity Index)'),
    (52, '太平洋区极涡强度指数 (Pacific Polar Vortex Intensity Index)'),
    (53, '北美区极涡强度指数 (North American Polar Vortex Intensity Index)'),
    (54, '北大西洋-欧洲区极涡强度指数 (Atlantic-European Polar Vortex Intensity Index)'),
    (55, '北半球极涡强度指数 (Northern Hemisphere Polar Vortex Intensity Index)'),
    (56, '北半球极涡中心经向位置指数 (Northern Hemisphere Polar Vortex Central Longitude Index)'),
    (57, '北半球极涡中心纬向位置指数 (Northern Hemisphere Polar Vortex Central Latitude Index)'),
    (58, '北半球极涡中心强度指数 (Northern Hemisphere Polar Vortex Central Intensity Index)'),
    (59, '欧亚纬向环流指数 (Eurasian Zonal Circulation Index)'),
    (60, '欧亚经向环流指数 (Eurasian Meridional Circulation Index)'),
    (61, '亚洲纬向环流指数 (Asian Zonal Circulation Index)'),
    (62, '亚洲经向环流指数 (Asian Meridional Circulation Index)'),
    (63, '东亚槽位置指数（East Asian Trough Position Index)'),
    (64, '东亚槽强度指数 (East Asian Trough Intensity Index)'),
    (65, '西藏高原-1指数 (Tibet Plateau Region 1 Index)'),
    (66, '西藏高原-2 指数（Tibet Plateau Region-2 Index)'),
    (67, '印缅槽强度指数 (India-Burma Trough Intensity Index)'),
    (68, '北极涛动指数（Arctic Oscillation, AO)'),
    (69, '南极涛动指数 (Antarctic Oscillation, AAO)'),
    (70, '北大西洋涛动指数 (North Atlantic Oscillation , NAO)'),
    (71, '太平洋-北美遥相关型指数 (Pacific/North American Pattern, PNA'),
    (72, '东大西洋遥相关型指数 (East Atlantic Pattern, EA)'),
    (73, '西太平洋遥相关型指数 (West Pacific Pattern , WP)'),
    (74, '北太平洋遥相关型指数 (North Pacific Pattern , NP)'),
    (75, '东大西洋-西俄罗斯遥相关型指数（East Atlantic-West Russia Pattern , EA/WR)'),
    (76, '热带-北半球遥相关型指数 (Tropical-Northern Hemisphere Pattern, TNH)'),
    (77, '极地-欧亚遥相关型指数 (Polar-Eurasia Pattern , POL)'),
    (78, '斯堪的纳维亚遥相关型指数 (Scandinavia Pattern , SCA)'),
    (79, '太平洋转换型指数（Pacific Transition Pattern, PT)'),
    (80, '30hPa纬向风指数 (30hPa zonal wind Index)'),
    (81, '50hPa纬向风指数（50 hPa zonal wind Index)'),
    (82, '赤道中东太平洋200hPa纬向风指数（Mid-Eastern Pacific 200mb Zonal Wind Index)'),
    (83, '850hPa西太平洋信风指数（West Pacific 850mb Trade Wind Index)'),
    (84, '850hPa中太平洋信风指数 ( Central Pacific 850mb Trade Wind Index)'),
    (85, '850hPa东太平洋信风指数 (East Pacific 850mb Trade Wind Index)'),
    (86, '北大西洋-欧洲环流W型指数（Atlantic-European Circulation W Pattern Index)'),
    (87, '北大西洋-欧洲环流型C型指数（Atlantic-European Circulation C Pattern Index)'),
    (88, '北大西洋-欧洲环流E型指数（Atlantic-European Circulation E Pattern Index)'),
])


Oce = OrderedDict([
    (1, 'NINO 1+2区海表温度距平指数（NINO 1+2 SSTA Index)'),
    (2, 'NINO 3区海表温度距平指数（NINO 3 SSTA Index)'),
    (3, 'NINO 4区海表温度距平指数（NINO 4 SSTA Index)'),
    (4, 'NINO 3.4区海表温度距平指数（NINO 3.4 SSTA Index)'),
    (5, 'NINO W区海表温度距平指数（NINO W SSTA Index)'),
    (6, 'NINO C区海表温度距平指数（NINO C SSTA Index)'),
    (7, 'NINO A区海表温度距平指数（NINO A SSTA Index)'),
    (8, 'NINO B区海表温度距平指数（NINO B SSTA Index)'),
    (9, 'NINO Z区海表温度距平海表温度指数（NINO Z SSTA Index)'),
    (10, '热带北大西洋海温指数（Tropical Northern Atlantic SST Index)'),
    (11, '热带南大西洋海温指数 (Tropical Southern Atlantic SST Index)'),
    (12, '西半球暖池指数 (Western Hemisphere Warm Pool Index)'),
    (13, '印度洋暖池面积指数 (Indian Ocean Warm Pool Area Index)'),
    (14, '印度洋暖池强度指数 (Indian Ocean Warm Pool Strength '),
    (15, '西太平洋暖池面积指数 (Western Pacific Warm Pool Area Index)'),
    (16, '西太平洋暖池强度指数(Western Pacific Warm Pool Strength index)'),
    (17, '大西洋多年代际振荡指数(Atlantic Multi-decadal Oscillation Index)'),
    (18, '亲潮区海温指数 (Oyashio Current SST Index)'),
    (19, '西风漂流区海温指数(West Wind Drift Current SST Index)'),
    (20, '黑潮区海温指数 (Kuroshio Current SST Index)'),
    (21, '类ENSO指数（ENSO Modoki Index)'),
    (22, '暖池型ENSO指数（Warm-pool ENSO Index)'),
    (23, '冷舌型ENSO指数（Cold-tongue ENSO Index)'),
    (24, '热带印度洋全区一致海温模态指数（Indian Ocean Basin-Wide Index)'),
    (25, '热带印度洋海温偶极子指数（Tropic Indian Ocean Dipole Index)'),
    (26, '副热带南印度洋偶极子指数（South Indian Ocean Dipole Index)'),
])


Ext = OrderedDict([
    (1, '冷空气次数（Cold Air Activity Index)'),
    (2, '西太平洋编号台风数（ Western North Pacific Typhoon number)'),
    (3, '登陆中国台风数（Number of Landing Typhoon on China)'),
    (4, '太阳黑子指数 (Total Sunspot Number Index)'),
    (5, '南方涛动指数 (Southern Oscillation Index)'),
    (6, '热带太平洋射出长波辐射指数 (Tropical Pacific Outgoing Long Wave Radiation Index)'),
    (7, '多变量ENSO指数（Multivariate ENSO Index)'),
    (8, '北太平洋年代际振荡指数（Pacific Decadal Oscillation Index)'),
    (9, '大西洋经向模海温指数（Atlantic Meridional Mode SST Index)'),
    (10, '准两年振荡指数（Quasi-Biennial Oscillation Index)'),
    (11, '全球综合角动量指数（Globally Integrated Angular Momentum Index)'),
    (12, '太阳辐射通量指数（Solar Flux Index)'),
    (13, '赤道太平洋130°E-80°W范围次表层海温指数（Equatorial Pacific 130°E-80°W Upper 300m temperature averaged anomaly index)'),
    (14, '赤道太平洋160°E-80°W范围次表层海温指数（Equatorial Pacific 160°E-80°W Upper 300m temperature Average anomaly index)'),
    (15, '赤道太平洋次表层海温指数（Equatorial Pacific 180º-100ºW Upper 300m temperature Average anomaly index)'),
    (16, '大西洋海温三极子指数（North Atlantic Triple index)'),
])


def wget(url, save_to_fn):
    data = urllib2.urlopen(url).read()
    with open(save_to_fn, 'w') as f:
        f.write(data)
    print save_to_fn


def update_climateIndex():
    for i in DATA_URLS:
        fn = DATA_URLS.get(i)
        url = BASE_URL + fn
        save_to_fn = os.path.join(BASE_DIR, 'climate_index', 'new88', fn)
        wget(url, save_to_fn)
        with open(save_to_fn, 'r') as f:
            data = f.readlines()
        data = [re.sub('\s+$', '', line) for line in data]
        data = [re.sub('\s+', ',', line) for line in data]
        data = '\n'.join(data)
        with open('{}.csv'.format(save_to_fn), 'w') as f:
            f.write(data)
    return True


def read_climateIndex(name):
    '''
        name in DATA_URLS
    '''
    fn = DATA_URLS.get(name)
    fn_fullpath = os.path.join(BASE_DIR, 'climate_index', 'new88', fn)
    data = pd.read_csv('{}.csv'.format(fn_fullpath), index_col=0)
    data[data==-999] = np.nan
    return data


def get_climateIndex_by_Month(data, month):
    '''
        data = read_climateIndex(name)
        month in [1, 2, ..., 12]
    '''
    return data[month-1::12]


def get_climateIndex_by_Col_and_Month(data, month, col):
    return data[col][month-1::12]


if __name__ == '__main__':
    update_climateIndex()
    # data = read_climateIndex('Oce')
    # s = get_climateIndex_by_Col_and_Month(data, 6, '5')
    # print s
    # s.plot()
    # plt.show()
