#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import matplotlib.gridspec as gridspec
from sklearn.cluster import DBSCAN
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import silhouette_score

from province import province
from read160data import *


years, datas = getData_by_names_season(ele='r', season='JJA', names=NAME)

# delete the last year, i.e. 2017
years = years[0][:-1]
data = [_[:-1] for _ in datas]

data_yearly = np.array(data).T
scaler = StandardScaler()
scaler.fit(data_yearly)
data_yearly = scaler.transform(data_yearly)
print(data_yearly.shape)


COLORS = ['r', 'g', 'b', 'y', 'm', 'c', 'Coral', 'burlywood', 'chartreuse', 'DarkKhaki', 'DarkOliveGreen']


def test(eps=0.3, min_samples=3):
    db = DBSCAN(eps=eps, min_samples=min_samples, metric='correlation', algorithm='brute').fit(data)
    labels = db.labels_
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

    print eps, min_samples, n_clusters_

    if n_clusters_ > 1:
        ss = silhouette_score(data, labels, metric='correlation')
        print('silhouette_score = {:.3f}'.format(ss))

        out = pd.DataFrame(dict(names=NAME, x=X, y=Y, d=data, label=labels))
        out.to_csv(os.path.join('DBSCAN_out', 'DBSCAN_out_{}_{}.csv'.format(eps, min_samples)), encoding='utf8')

        fig = plt.figure(figsize=(18, 7))
        gs = gridspec.GridSpec(nrows=n_clusters_, ncols=2, height_ratios=[1,]*n_clusters_)

        ax0 = fig.add_subplot(gs[:, 0])
        for _ in range(-1, n_clusters_):
            it = out[out['label']==_]
            if _ == -1:
                ax0.plot(it.x, it.y, 'o', color='none', label='Outlier')
                for p in province:
                    px,py = [[i[j] for i in p] for j in (0,1)]
                    ax0.plot(px, py, '0.618', linewidth=0.3)
                plt.title('silhouette_score = {:.3f}'.format(ss))
            else:
                ax0.plot(it.x, it.y, 'o', color=COLORS[_], label='Cluster {}'.format(_))
                axn = fig.add_subplot(gs[_, 1])
                axn.plot(years, np.array(it.d.tolist()).T, '-', color='0.618')
                axn.plot(years, np.array(it.d.tolist()).T.mean(axis=1), '-', color=COLORS[_])
        ax0.legend(loc='best')
        plt.savefig(os.path.join('DBSCAN_out', 'DBSCAN_out_{}_{}.png'.format(eps, min_samples)), dpi=100)
        plt.clf()


def test_yearly_cluster(eps=0.3, min_samples=10):

    db = DBSCAN(eps=eps, min_samples=min_samples, metric='correlation', algorithm='brute').fit(data_yearly)
    labels = db.labels_
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

    print eps, min_samples, n_clusters_

    if n_clusters_ > 1:
        ss = silhouette_score(data_yearly, labels, metric='correlation')
        print('silhouette_score = {:.3f}'.format(ss))

        out = pd.DataFrame(dict(years=years, label=labels))
        out.to_csv(os.path.join('DBSCAN_out_yearly_cluster', 'DBSCAN_out_{}_{}.csv'.format(eps, min_samples)), encoding='utf8')


def main():
    for eps in [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
        for min_samples in range(3, 19):
            test(eps, min_samples)


def main_yearly_cluster():
    for eps in [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
        for min_samples in range(3, 19):
            test_yearly_cluster(eps, min_samples)
    

if __name__ == '__main__':
    # test()
    # main()
    main_yearly_cluster()
