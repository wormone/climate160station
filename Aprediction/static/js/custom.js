// 地图初始化
var normalm = L.tileLayer.chinaProvider('TianDiTu.Normal.Map', {
        maxZoom: 7,
        minZoom: 5
    }),
    normala = L.tileLayer.chinaProvider('TianDiTu.Normal.Annotion', {
        maxZoom: 7,
        minZoom: 5
    }),
    imgm = L.tileLayer.chinaProvider('TianDiTu.Satellite.Map', {
        maxZoom: 7,
        minZoom: 5
    }),
    imga = L.tileLayer.chinaProvider('TianDiTu.Satellite.Annotion', {
        maxZoom: 7,
        minZoom: 5
    });

var normal = L.layerGroup([normalm, normala]),
    image = L.layerGroup([imgm, imga]);

var baseLayers = {
    "地图": normal,
    "影像": image,
}

var overlayLayers = {

}

var map = L.map("map", {
    center: [36, 115],
    zoom: 5,
    layers: [normal],
    zoomControl: false
});

L.control.layers(baseLayers, overlayLayers).addTo(map);
L.control.zoom({
    zoomInTitle: '放大',
    zoomOutTitle: '缩小'
}).addTo(map);


// 设定日期选择器
$.fn.datepicker.defaults.todayHighlight = true;
$.fn.datepicker.defaults.language = "zh-CN";
$.fn.datepicker.defaults.format = "yyyy-mm-dd";
$("#date-select-obs-start").datepicker({
    startDate: '-15d',
    endDate: '-1d',
});
$("#date-select-obs-end").datepicker({
    startDate: '-15d',
    endDate: '-1d',
});
$("#date-select-fcst").datepicker({
    startDate: '-15d',
    endDate: '0d',
});


// 点击切换状态
$("#obs").click(function(){
    $(this).removeClass('btn-default').addClass('btn-primary');
    $("#fcst").removeClass('btn-primary').addClass('btn-default');
    $("#date-obs").show();
    $("#date-fcst").hide();
});
$("#fcst").click(function(){
    $(this).removeClass('btn-default').addClass('btn-primary');
    $("#obs").removeClass('btn-primary').addClass('btn-default');
    $("#date-fcst").show();
    $("#date-obs").hide();
});
$(".item-name").click(function(){
    $(".item-name").removeClass('btn-primary').addClass('btn-default');
    $(this).removeClass('btn-default').addClass('btn-primary');
});

$(".season-name").click(function(){
    $(".season-name").removeClass('btn-primary').addClass('btn-default');
    $(this).removeClass('btn-default').addClass('btn-primary');
});
