#!/usr/bin/env python
# -*- coding: utf-8 -*-

from stations import *

import geojson
from geojson import Feature, Point, FeatureCollection


features = list(range(len(STID)))
for i in features:
    features[i] = Feature(geometry=Point((X[i], Y[i])), id=ID[i],
                          properties=dict(name=NAME[i], stid=STID[i]))
features = FeatureCollection(features)
features = geojson.dumps(features, sort_keys=True)
with open('stations.json', 'w') as s:
    s.write(features)

