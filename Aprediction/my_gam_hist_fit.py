# coding:utf8

from __future__ import unicode_literals

import os
import numpy as np
from statsmodels.distributions.empirical_distribution import ECDF
from scipy.stats import gamma
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


THIS_DIR = os.path.dirname(os.path.abspath(__file__))
MYFONTFILE = os.path.join(THIS_DIR, 'msyh.ttc')
MYFONT_sm, MYFONT, MYFONT_lg = [
    matplotlib.font_manager.FontProperties(fname=MYFONTFILE, size=size) 
    for size in (9, 12, 15)
]


def my_gam_hist_fit(data, npoint=20):
    ''' gamma distribution fit, pdf and cdf plots '''
    npoint = int(np.floor(npoint))
    npoint = 20 if npoint < 10 or npoint > 30 else npoint

    # bins = np.linspace(min(data), max(data), npoint)
    x = np.linspace(min(data), max(data), 100)
    # epdf,bin_edges = np.histogram(data, bins=npoint, density=True)
    # delta = bin_edges[1] - bin_edges[0]
    ecdf = ECDF(data)
    fshape, floc, fscale = gamma.fit(data)

    plt.hold(True)
    plt.hist(data, bins=npoint, normed=True, facecolor='k', alpha=0.2, align='mid', label='频次统计')
    plt.plot(x, gamma.pdf(x, fshape, floc, fscale), color='r', linewidth=2, label='概率分布拟合')
    plt.xlabel('冬季长度（天）', fontproperties=MYFONT)
    plt.ylabel('概率密度', fontproperties=MYFONT)
    plt.legend(loc='upper right', prop=MYFONT)
    plt.savefig('pdf.png')
    plt.clf()

    plt.hold(True)
    plt.step(ecdf.x, ecdf.y, color='k', label='频次统计')
    plt.plot(x, gamma.cdf(x, fshape, floc, fscale), color='r', linewidth=2, label='概率分布拟合')
    plt.ylim([0, 1])
    plt.xlabel('冬季长度（天）', fontproperties=MYFONT)
    plt.ylabel('累积概率密度', fontproperties=MYFONT)
    plt.legend(loc='lower right', prop=MYFONT)
    plt.savefig('cdf.png')
    plt.clf()


if __name__ == '__main__':
	data = gamma.rvs(5, loc=110, scale=5, size=65)
	my_gam_hist_fit(data, npoint=20)
