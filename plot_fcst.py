#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from matplotlib.path import Path
from matplotlib.patches import PathPatch

from stationSeries_corr_climateIndex import *


site = '岷县'

ele = 'r'
period = '04'
isAbnorm = True
from_year = 1952
to_year = 2017
grade_method = 'fixed'


prob_1 = [0.1, 0.06, 0.06, 0.25, 0.13, 0.4]    # 2017
prob_2 = [0.04, 0.34, 0.23, 0.11, 0.25, 0.03]  # 2016
prob_3 = [0.06, 0.04, 0.16, 0.23, 0.31, 0.2]   # 2015
prob_4 = [0.2, 0.04, 0.17, 0.09, 0.43, 0.07]   # 2014
prob_5 = [0.12, 0.09, 0.07, 0.19, 0.19, 0.34]  # 2013

prob_0 = [0.13, 0.02, 0.14, 0.16, 0.4, 0.15]   # 2018


prob_all = [prob_5, prob_4, prob_3, prob_2, prob_1, prob_0]
year_prob = range(to_year-4, to_year+2)


year_origin = 1950


y_grade = read_to_grade(ele, period, site, 
    grade_method=grade_method, isAbnorm=isAbnorm, from_year=from_year, to_year=to_year)
y, grade, norm = [y_grade[_] for _ in ['y', 'grade', 'norm']]
divider = y_grade['theGrades']['divider']

y_raw = (np.array(y) * 0.01 + 1) * norm
dv = (np.array(divider) * 0.01 + 1) * norm


y_min = 0
y_max = 120


codes = [Path.MOVETO] + [Path.LINETO]*3 + [Path.CLOSEPOLY]

op = (year_origin, y_min)

fig, ax = plt.subplots()


for _ in range(len(year_prob)):
    yl, yr = year_prob[_] - 0.5, year_prob[_] + 0.5
    prob = prob_all[_]

    for i in range(len(dv) + 1):
        rec_0 = [(yl, y_min), (yr, y_min), (yr, dv[0]), (yl, dv[0]), op]
        rec_1 = [(yl, dv[0]), (yr, dv[0]), (yr, dv[1]), (yl, dv[1]), op]
        rec_2 = [(yl, dv[1]), (yr, dv[1]), (yr, dv[2]), (yl, dv[2]), op]
        rec_3 = [(yl, dv[2]), (yr, dv[2]), (yr, dv[3]), (yl, dv[3]), op]
        rec_4 = [(yl, dv[3]), (yr, dv[3]), (yr, dv[4]), (yl, dv[4]), op]
        rec_5 = [(yl, dv[4]), (yr, dv[4]), (yr, y_max), (yl, y_max), op]
        rec = [rec_0, rec_1, rec_2, rec_3, rec_4, rec_5][i]
        rec = np.array(rec, float)
        path = Path(rec, codes)
        pathpatch = PathPatch(path, facecolor='blue', edgecolor='k', alpha=prob[i])
        ax.add_patch(pathpatch)


plt.plot(range(from_year, to_year + 1), y_raw, 'ko-')
plt.show()
