#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2016-07-21 10:24:53
# @Author  : wormone (renyuuu@gmail.com)

from __future__ import unicode_literals

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats
from collections import OrderedDict
# from sklearn import linear_model, svm, tree
from sklearn.model_selection import cross_val_score, StratifiedKFold
from sklearn.ensemble import (BaggingClassifier, BaggingRegressor, 
    RandomForestClassifier, RandomForestRegressor, 
    ExtraTreesClassifier, ExtraTreesRegressor, 
    AdaBoostClassifier, AdaBoostRegressor, 
    GradientBoostingClassifier, GradientBoostingRegressor)
import multiprocessing


import read160data as R160s
import download_climateIndex as CI


FCST_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'fcst')


CORR_FUNC = {
    'pearson': stats.pointbiserialr,
    'spearman': stats.spearmanr,
    'kendall': stats.kendalltau,
}


CLF = {
    'bagging': (BaggingClassifier, BaggingRegressor),
    'forest': (RandomForestClassifier, RandomForestRegressor),
    'extratree': (ExtraTreesClassifier, ExtraTreesRegressor),
    'adaboost': (AdaBoostClassifier, AdaBoostRegressor),
    'gradient': (GradientBoostingClassifier, GradientBoostingRegressor),
}


PARAMS = {
    'n_estimators': 100,
    'random_state': 0,
    # 'max_depth': None,
    # 'min_samples_split': 1,
}


def temperature_abnorm_to_grade(temperature_abnorm):
    '''
        temperature_abnorm in list or np.array
    '''
    temperature_abnorm = list(temperature_abnorm)
    grade = list(range(len(temperature_abnorm)))
    for g in grade:
        abn = temperature_abnorm[g]
        if abn >= 1:
            grade[g] = 5
        elif 0.5 <= abn < 1:
            grade[g] = 4
        elif 0 <= abn < 0.5:
            grade[g] = 3
        elif -0.5 < abn < 0:
            grade[g] = 2
        elif -1 < abn <= -0.5:
            grade[g] = 1
        elif abn <= -1:
            grade[g] = 0
    divider = [-1, -0.5, 0, 0.5, 1]
    grade_name = range(6)
    scope = [[-np.inf, -1], [-1, -0.5], [-0.5, 0], [0, 0.5], [0.5, 1], [1, np.inf]]
    scope = pd.DataFrame({'scope': scope}, index=grade_name)
    return {'divider': divider, 'scope': scope, 'grade': grade}


def abnorm_percent_to_grade(abnorm_percent):
    '''
        abnorm_percent in list or np.array
    '''
    abnorm_percent = list(abnorm_percent)
    grade = list(range(len(abnorm_percent)))
    for g in grade:
        abn = abnorm_percent[g]
        if abn >= 50:
            grade[g] = 5
        elif 20 <= abn < 50:
            grade[g] = 4
        elif 0 <= abn < 20:
            grade[g] = 3
        elif -20 < abn < 0:
            grade[g] = 2
        elif -50 < abn <= -20:
            grade[g] = 1
        elif abn <= -50:
            grade[g] = 0
    divider = [-50, -20, 0, 20, 50]
    grade_name = range(6)
    scope = [[-np.inf, -50], [-50, -20], [-20, 0], [0, 20], [20, 50], [50, np.inf]]
    scope = pd.DataFrame({'scope': scope}, index=grade_name)
    return {'divider': divider, 'scope': scope, 'grade': grade}


def grade_by_percentile(data, percentile=[16.67, 33.33, 50, 66.67, 83.33]):
    '''
        data in list or np.array
        grade the data by the given percentile, 
        For example, if percentile=[20, 40, 60, 80], then
            [0%-20%, 20%-40%, 40%-60%, 60%-80%, 80%-100%] in grades of [0, 1, 2, 3, 4]
        While percentile=[25, 40, 50, 60, 75], 
            equivalent to the scope FIXED in abnorm_percent_to_grade()
            [0%-25%, 25%-40%, 40%-50%, 50%-60%, 60%-75%, 75%-100%] in grades of [0, 1, 2, 3, 4, 5]
    '''
    data = list(data)
    divider = np.percentile(data, percentile)
    grade_name = range(len(percentile) + 1)
    scope = list(grade_name)
    for s in scope:
        if s == 0:
            scope[s] = [-np.inf, divider[s]]
        if s == len(percentile):
            scope[s] = [divider[s - 1], np.inf]
        if 0 < s < len(percentile):
            scope[s] = [divider[s - 1], divider[s]]
    s_low, s_high = [[i[_] for i in scope] for _ in range(2)]
    s_low, s_high = [np.array(_) for _ in [s_low, s_high]]
    grade = list(range(len(data)))
    for g in grade:
        d = data[g]
        True_of_False = ((d - s_low) * (d - s_high) <= 0).tolist()
        grade[g] = True_of_False.index(True)
    scope = pd.DataFrame({'scope': scope}, index=grade_name)
    return {'divider': divider, 'scope': scope, 'grade': grade}


GRADE_METHOD = {
    'fixed': abnorm_percent_to_grade,
    'auto': grade_by_percentile,
    'fixed_temperature': temperature_abnorm_to_grade,
}


def read_to_grade(ele, period, site, grade_method='auto', isAbnorm=True, 
                  from_year=1952, to_year=2015, isPrint=True, **kw):
    '''
        READING data in raw or abnormal for single site for: 
            ele in ['t', 'r'],
            period in month or season,
            grade_method in ['auto', 'fixed', 'fixed_temperature'], 'auto' for grade_by_percentile() and 
                'fixed' and 'fixed_temperature' for abnorm_percent_to_grade
        OUTPUTS(packed in dict):
            'y': raw data or abnorm_percent in pd.DataFrame, indexed by year
            'theGrades': a dict returned by grade_by_percentile()
            'grade': grade of y, in pd.DataFrame, indexed by year
            'norm': 1981_2010_means if isAbnorm else None
    '''
    isAbnorm = True if grade_method in ['fixed', 'fixed_temperature'] else isAbnorm # force isAbnorm to be True if 'fixed'
    try:
        int(period)
        is_season = False
    except ValueError:
        is_season = True
    norm = None
    if isAbnorm:
        read_func = R160s.getAbnorm_by_name_season if is_season else \
            R160s.getAbnorm_by_name
        year, norm, abnorm, abnorm_percent = read_func(ele, period, site)
        df_station = pd.DataFrame({
            'abnorm_percent': abnorm_percent,
            'abnorm': abnorm,
            'norm': norm}, index=year)
        y = df_station.ix[from_year:to_year, 'abnorm_percent' if grade_method=='fixed' else 'abnorm'] # not good but work
    else:
        read_func = R160s.getData_by_name_season if is_season else \
            R160s.getData_by_name
        year, data = read_func(ele, period, site)
        df_station = pd.DataFrame({'data': data}, index=year)
        y = df_station.ix[from_year:to_year, 'data']
    grade_func = GRADE_METHOD.get(grade_method)
    theGrades = grade_func(y)
    grade = pd.DataFrame({'real': theGrades['grade']}, index=y.index)
    if isPrint:
        Inputs = {'ele': ele, 'period': period, 'site': site, 'isAbnorm': isAbnorm,
                  'from_year': from_year, 'to_year': to_year}
        print '================================> READ and GRADE the data'
        for k, v in Inputs.iteritems():
            print 'Input {} = {}'.format(k, v)
        print 'Data divider = {}'.format(theGrades['divider'])
    return {'y': y, 'theGrades': theGrades, 'grade': grade, 'norm': norm}


def filter_factors(y, from_last_year_month, to_this_year_month,
                   Scopes=['Atm', 'Oce', 'Ext'], sig=0.01, **kw):
    '''
        INPUTS:
            y, in pd.DataFrame and indexed by year
            from_last_year_month, i.e. the beginning month of the factors to be filtered in LAST year
            to_this_year_month, i.e. the last month of the factors to be filtered in THIS year
            Scopes, i.e. the name spaces of the factors that would be filtered
            sig, i.e. the significance level for correlation analysis
        OUTPUTS:
            Col_Month_Name_Lag, in list and len(Col_Month_Name_Lag)=number_of_filtered_factors,
            element in Col_Month_Name_Lag like ['5', 12, 'Atm', -1],
            Col, Month, Name, Lag = ['5', 12, 'Atm', -1],
            Col as the factor id, Month as the month of the factor, Name as the name_space,
            Lag =-1 for last year and =0 for this year
    '''
    Inputs = {'from_last_year_month': from_last_year_month,
              'to_this_year_month': to_this_year_month,
              'Scopes': Scopes}
    print '================================> FILTER the factors'
    for k, v in Inputs.iteritems():
        print 'Input {} = {}'.format(k, v)
    y_begin_year, y_end_year = [list(y.index)[_] for _ in [0, -1]]
    # define a subroutine to filter factors
    def filter(lag, months, Scopes=Scopes, sig=sig):
        ''' Usage:
            X_1 = filter(lag=-1, months=range(from_last_year_month, 13), 
                Scopes=Scopes, sig=sig)
            X_0 = filter(lag=0, months=range(1, to_this_year_month + 1), 
                Scopes=Scopes, sig=sig)
        '''
        X = pd.DataFrame()
        month_from, month_to = 100*(y_begin_year+lag)+1, 100*(y_end_year+lag)+12
        for xScope in Scopes:
            for xMonth in months:
                data = CI.read_climateIndex(xScope)
                df_climate = CI.get_climateIndex_by_Month(data, xMonth)
                for col in df_climate.columns:
                    x = df_climate.ix[month_from:month_to, col]
                    for func in CORR_FUNC:
                        try:
                            r, p = CORR_FUNC[func](x, y)
                        except ValueError as e:
                            r, p = np.nan, np.nan
                        if func in ['pearson', 'spearman'] and p < sig:  # ['pearson', 'spearman']
                            X = pd.concat([X, pd.DataFrame({
                                'Name': xScope,
                                'Month': xMonth,
                                'Col': col,
                                'func': func,
                                'r': round(r, 3),
                                'p': round(p, 3),
                                'Lag': lag,
                            }, index=[lag,])])
        return X
    # filter factors for last year and this year
    X_1 = filter(lag=-1, months=range(from_last_year_month, 13 + to_this_year_month if to_this_year_month < 0 else 13),  # i.e. -1 --> month end with 11
                 Scopes=Scopes, sig=sig)
    X_0 = filter(lag=0, months=range(1, to_this_year_month + 1), 
                 Scopes=Scopes, sig=sig)
    X = pd.concat([X_1, X_0])
    # print 'Filterd factors by correlation:'
    # print X
    Xid = X.apply(lambda x: "{}_{}_{}_{}".format(
        x.get('Col'), x.get('Month'), x.get('Name'), x.get('Lag')), axis=1)
    Col_Month_Name_Lag = []
    for xid in set(Xid):
        if list(Xid).count(xid) == 2:
            xid = xid.split('_')
            xid = [xid[0], int(xid[1]), xid[2], int(xid[3])]
            Col_Month_Name_Lag.append(xid)
    if len(Col_Month_Name_Lag) < 1:
        print 'Warning: No significant factors for Pearson and Spearman ' + \
              'in the meantime as sig={}'.format(sig)
        for xid in set(Xid):
            xid = xid.split('_')
            xid = [xid[0], int(xid[1]), xid[2], int(xid[3])]
            Col_Month_Name_Lag.append(xid)
    ''' Re-filter factors by downgrade sig '''
    while len(Col_Month_Name_Lag) < 3: # only ONE factor might contains NaNs if filtered by Spearman
        sig *= 2
        print 'Re-filter factors by downgrade sig to {}'.format(sig)
        Col_Month_Name_Lag = filter_factors(y, sig=sig, **Inputs)
    # print 'Finally filterd factors:'
    # print Col_Month_Name_Lag
    return Col_Month_Name_Lag


def get_predictors(years, Col_Month_Name_Lag, isGraded=False):
    '''
        INPUTS:
            years, in list like range(1952, 2015+1)
            Col_Month_Name_Lag, returned by filter_factors()
        OUTUTS:
            predictors, in np.ndarray, with shape as (n_samples, n_features),
            i.e. (n_years, n_factors)
    '''
    begin_year, end_year = [years[_] for _ in [0, -1]]
    predictors, col_month_name_lag = [], []
    for item in Col_Month_Name_Lag:
        col, month, name, lag = item
        month_from, month_to = 100*(begin_year+lag)+1, 100*(end_year+lag)+12
        data = CI.read_climateIndex(name)
        df_climate = CI.get_climateIndex_by_Month(data, month)
        x = list(df_climate.ix[month_from:month_to, col])
        if not np.isnan(sum(x)):
            x = grade_by_percentile(x)['grade'] if isGraded else x
            predictors.append(x)
            col_month_name_lag.append(item)
        else:
            # print '!!! Alert: NaN found in {}'.format(item)
            # print '!!! factors filtered by Pearson Correlation ignore NaNs'
            pass
    predictors = np.array(predictors).T
    print '================================> GET the predictors'
    print 'years from {} to {}'.format(years[0], years[-1])
    print 'isGraded = {}'.format(isGraded)
    print 'Nyears, Nfactors = {}'.format(predictors.shape)
    return {'predictors': predictors, 'col_month_name_lag': col_month_name_lag}


def validate_prediction(y, grade, predictors, clf='extratree', nValid=-5):
    '''
        INPUTS:
            y, in pd.DataFrame and indexed by year
            grade, in a dict returned by grade_by_percentile()
            predictors, returned by get_predictors()
            clf, in global var CLF, classifier model
        OUTPUTS(packed in dict):
            'summary', a pd.DataFrame includes prediction information for nValid
            'scores', socres for classifier and regressor
    '''
    real = grade[nValid:]
    grades = list(grade['real'])
    classifier, regressor = [func().set_params(**PARAMS) for func in CLF[clf]]
    scores = [cross_val_score(estimator=extimator, X=predictors, y=grades, cv=3)
              for extimator in [classifier, regressor]]
    scores = {'classifier': scores[0], 'regressor': scores[1]}
    classifier.fit(X=predictors[:nValid], y=grades[:nValid])
    fcst_grade = classifier.predict(X=predictors[nValid:])
    fcst_proba = classifier.predict_proba(X=predictors[nValid:])
    fcst_grade = pd.DataFrame({'fcst_grade': fcst_grade}, index=real.index)
    # grade which does NOT really exist, will still be 0 of probability and not estimated
    fcst_proba = pd.DataFrame(fcst_proba, 
        columns=sorted(list(set(grades[:nValid]))), index=real.index).round(2)
    regressor.fit(X=predictors[:nValid], y=y[:nValid])
    fcst_y = regressor.predict(X=predictors[nValid:])
    fcst_y = pd.DataFrame({'fcst_y': fcst_y}, index=real.index).round(1)
    summary = pd.concat([y[nValid:], fcst_y, real, fcst_grade, fcst_proba], axis=1)
    print '================================> VALIDATE the prediction'
    print classifier
    for s in scores.keys():
        score = scores[s]
        print '{} Cross_val_core = {}, mean as {:.3f}'.format(
            s.upper(), np.round(score, 3), score.mean())
    print summary
    return {'summary': summary, 'scores': scores}


def make_fcst(y, grade, years_train, Col_Month_Name_Lag, years_fcst, clf='extratree'):
    '''
        INPUTS:
            y, returned by read_to_grade()
            grade, returned by read_to_grade()
            years_train, like range(1952, 2014+1)
            Col_Month_Name_Lag, returned by filter_factors()
            years_fcst, like [2015], in list
        OUTPUTS(packed in pd.DataFrame):
            'fcst_y', forecasted y
            'fcst_grade', forecsted grade
            0,1,2,3,4, forecasted probability for each grade
    '''
    X_all = get_predictors(years_train + years_fcst, Col_Month_Name_Lag)['predictors']
    X_train, X_fcst = X_all[:len(years_train), :], X_all[len(years_train):, :]
    ''' get X_all for X_train and X_fcst, instead of get them separately
    # train = get_predictors(years_train, Col_Month_Name_Lag)
    # X_train, col_month_name_lag = [train[_] for _ in ['predictors', 'col_month_name_lag']]
    # X_fcst = get_predictors(years_fcst, col_month_name_lag)['predictors']
    '''
    y_train, y_grade_train = [list(_.ix[years_train]) for _ in [y, grade['real']]]
    classifier, regressor = [func().set_params(**PARAMS) for func in CLF[clf]]
    classifier.fit(X=X_train, y=y_grade_train)
    fcst_grade = classifier.predict(X=X_fcst)
    fcst_proba = classifier.predict_proba(X=X_fcst)
    regressor.fit(X=X_train, y=y_train)
    fcst_y = regressor.predict(X=X_fcst)
    fcst_grade = pd.DataFrame({'fcst_grade': fcst_grade}, index=years_fcst)
    # grade which does NOT really exist, will still be 0 of probability and not estimated
    fcst_proba = pd.DataFrame(np.round(fcst_proba, 2), 
        columns=sorted(list(set(y_grade_train))), index=years_fcst)
    fcst_y = pd.DataFrame({'fcst_y': np.round(fcst_y, 1)}, index=years_fcst)
    fcsted = pd.concat([fcst_y, fcst_grade, fcst_proba], axis=1)
    print '================================> MAKE forecast'
    print classifier
    print 'years_train from {} to {}, altogether {} years'.format(
        years_train[0], years_train[-1], len(years_train))
    print 'years_fcst = {}'.format(years_fcst)
    print fcsted
    return fcsted


def fcst_one_site(ele='r', period='JJA', site='杭州',
                  from_last_year_month = 7, to_this_year_month = 4,
                  from_year = 1952, to_year = 2015,
                  grade_method='auto', isAbnorm=True,
                  clf='extratree', nValid=-5, sig=0.01, **kw):
    '''
        OUTPUTS:
            validation returned from validate_prediction()
            fcsted returned from make_fcst()
            divider and norm returned from read_to_grade()
    '''
    years_train = range(from_year, to_year + 1)

    y_grade = read_to_grade(ele, period, site, 
        grade_method=grade_method, isAbnorm=isAbnorm, from_year=from_year, to_year=to_year)
    y, grade, norm = [y_grade[_] for _ in ['y', 'grade', 'norm']]
    divider = y_grade['theGrades']['divider']
    Col_Month_Name_Lag = filter_factors(y, from_last_year_month, to_this_year_month, sig=sig, **kw)

    predictors = get_predictors(years_train, Col_Month_Name_Lag)['predictors']
    validation = validate_prediction(y, grade, predictors, clf=clf, nValid=nValid)

    years_fcst = [to_year + 1]  # fcst next year
    fcsted = make_fcst(y, grade, years_train, Col_Month_Name_Lag, years_fcst, clf=clf)

    return validation, fcsted, divider, norm


def fcst_160_stations(grade_method='auto', clf='extratree', **kw):
    sites = R160s.NAME
    Summary, Fcsted = [list(range(len(sites))) for _ in range(2)]
    for i in Summary:
        site = sites[i]
        validation, fcsted, divider, norm = fcst_one_site(
            site=site, grade_method=grade_method, clf=clf, **kw)
        summary = validation['summary']
        summary['site'] = site
        for _ in ['classifier', 'regressor']:
            fcsted['{}_score'.format(_)] = validation['scores'][_].mean().round(2)
        fcsted['site'] = site
        fcsted['divider'] = [divider]  # len(divider)=4, len([divider])=1
        fcsted['norm'] = norm
        Summary[i], Fcsted[i] = summary, fcsted
    Summary, Fcsted = [pd.concat(_) for _ in [Summary, Fcsted]]
    Summary.to_csv(os.path.join(FCST_DIR, 
        'Validation_{}_{}.csv'.format(grade_method, clf)), encoding='utf8')
    Fcsted.to_csv(os.path.join(FCST_DIR, 
        'Fcsted_{}_{}.csv'.format(grade_method, clf)), encoding='utf8')
    return Summary, Fcsted


"""
    采用多进程的方式改写 fcst_160_stations
    需要注意的是，被多线程调用的函数不能采用 lambda 匿名函数的方式，而且必须作为全局函数
    once 定义在 fcst_160_stations_parallel 中也是不行的！
"""
def once(site, grade_method='auto', clf='extratree', **kw):

    validation, fcsted, divider, norm = fcst_one_site(
        site=site, grade_method=grade_method, clf=clf, **kw)
    summary = validation['summary']
    summary['site'] = site
    for _ in ['classifier', 'regressor']:
        fcsted['{}_score'.format(_)] = validation['scores'][_].mean().round(2)
    fcsted['site'] = site
    fcsted['divider'] = [divider]  # len(divider)=4, len([divider])=1
    fcsted['norm'] = norm
    return summary, fcsted

    # try:
    #     validation, fcsted, divider, norm = fcst_one_site(
    #         site=site, grade_method=grade_method, clf=clf, **kw)
    #     summary = validation['summary']
    #     summary['site'] = site
    #     for _ in ['classifier', 'regressor']:
    #         fcsted['{}_score'.format(_)] = validation['scores'][_].mean().round(2)
    #     fcsted['site'] = site
    #     fcsted['divider'] = [divider]  # len(divider)=4, len([divider])=1
    #     fcsted['norm'] = norm
    #     return summary, fcsted
    # except Exception as e:
    #     print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
    #     print site, grade_method, clf
    #     print e
    #     print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
    #     return None, None


def fcst_160_stations_parallel(grade_method='auto', clf='extratree', **kw):
    pool = multiprocessing.Pool(processes=7)
    Summary_Fcsted = []
    for site in R160s.NAME:
        Summary_Fcsted.append(pool.apply_async(once, args=(site, grade_method, clf), kwds=kw))
    pool.close()
    pool.join()
    Summary, Fcsted = [[_.get(timeout=100)[i] for _ in Summary_Fcsted] for i in (0, 1)]
    Summary, Fcsted = [[what for what in _ if what is not None] for _ in [Summary, Fcsted]]
    Summary, Fcsted = [pd.concat(_) for _ in [Summary, Fcsted]]
    Summary.to_csv(os.path.join(FCST_DIR, 
        'Validation_{}_{}.csv'.format(grade_method, clf)), encoding='utf8')
    Fcsted.to_csv(os.path.join(FCST_DIR, 
        'Fcsted_{}_{}.csv'.format(grade_method, clf)), encoding='utf8')
    return Summary, Fcsted


if __name__ == '__main__':
    # for grade_method in GRADE_METHOD:
    #     fcst_one_site(site='图里河', grade_method=grade_method)
    #     for clf in CLF:
    #         fcst_160_stations(grade_method=grade_method, clf=clf)

    kw = dict(
        period = 'JJA',
        from_last_year_month = 7,
        to_this_year_month = 2,
        from_year = 1952,
        to_year = 2016,
    )
    for grade_method in ['fixed', 'auto']:
        fcst_one_site(site='吐鲁番', grade_method=grade_method, **kw)
        # for clf in CLF:
        #     fcst_160_stations_parallel(grade_method=grade_method, clf=clf, **kw)
