# coding:utf8

from ispointinpoly import point_in_poly
from china import china


# the last poly in china is the main land
china = china[::-1]


def point_in_china(pt,china=china):
    ''' pt = [lon,lat] '''
    IN = False
    for poly in china:
        if point_in_poly(pt,poly):
            IN = True; return IN; break


def points_in_china(pts):
    ''' pts = [[x0,y0],[x1,y1],...,[xn,yn]] '''
    INS = list(range(len(pts)))
    for i in INS:
        INS[i] = point_in_china(pts[i])
    return INS