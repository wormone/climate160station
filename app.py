# coding:utf8

import os
from flask import Flask, render_template, request, session, url_for, redirect,\
    flash, abort, make_response, Markup, escape, jsonify
from flask_wtf.csrf import CsrfProtect

from plot160data import *


SECRET_KEY = os.urandom(24)
CSRF_ENABLED = True
app = Flask(__name__)
app.config.from_object(__name__)
CsrfProtect(app)


PERIODS = {
    '-4': 'DJF',
    '-3': 'MAM',
    '-2': 'JJA',
    '-1': 'SON',
    '0': 'YEAR',
}


@app.route('/')
def index():
    return render_template('home.html')


# only POST for production, GET for rest API
@app.route('/_do', methods=['GET', 'POST'])
def do():
    try:
        ele, output, period, chartmap, yearsmean, stationsmean = [
            request.values.get(x) for x in (
                'ele', 'output', 'period', 'chartmap', 'yearsmean', 'stationsmean'
            )
        ]
        season, month = None, None
        if int(period) < 1:
            season = PERIODS.get(period)
        else:
            month = '0' * (2 - len(period)) + period
        years, names = None, None
        if chartmap == 'map':
            yearsmean = yearsmean.strip().split(' ')
            years = [int(i) for i in yearsmean]
        if chartmap == 'line':
            stationsmean = stationsmean.strip().split(' ')
            stids = [int(i) for i in stationsmean]
            names = [getName_by_stid(i) for i in stids]

        result = {}
        pngfilename = None
        if years:
            abnorms, abnorm_percents = [None for i in range(2)]
            if output == 'raw':
                result = {
                    'Error': 'Sorry, Method NOT defined for this function.'}
                return jsonify(result)
            if month:
                abnorms, abnorm_percents = meanAbnorm_mon_by_names_years(
                    ele, month, NAME, years)
            if season:
                abnorms, abnorm_percents = meanAbnorm_season_by_names_years(
                    ele, season, NAME, years)
            if output == 'abnorm':
                pngfilename = plot160data(ele, abnorms)
            if output == 'abnorm-percent':
                pngfilename = plot160data(ele, abnorm_percents)
            result.update({'name': NAME, 'abnorms': abnorms,
                           'abnorm_percents': abnorm_percents})
        if names:
            years, datas, abnorms, abnorm_percents = [None for i in range(4)]
            if month:
                if output == 'raw':
                    years, datas = getData_by_names(ele, month, names)
                    pngfilename = plotline(ele, years, datas)
                else:
                    years, norms, abnorms, abnorm_percents = getAbnorm_by_names(
                        ele, month, names)
                    if output == 'abnorm':
                        pngfilename = plotline(ele, years, abnorms)
                    if output == 'abnorm-percent':
                        pngfilename = plotline(ele, years, abnorm_percents)
            if season:
                if output == 'raw':
                    years, datas = getData_by_names_season(ele, season, names)
                    pngfilename = plotline(ele, years, datas)
                else:
                    years, norms, abnorms, abnorm_percents = getAbnorm_by_names_season(
                        ele, season, names)
                    if output == 'abnorm':
                        pngfilename = plotline(ele, years, abnorms)
                    if output == 'abnorm-percent':
                        pngfilename = plotline(ele, years, abnorm_percents)
            result.update({'years': years, 'datas': list(
                datas), 'abnorms': abnorms, 'abnorm_percents': abnorm_percents})
        url_pngfile = url_for(
            'static', filename='images/' + pngfilename, _external=True)
        result.update({'url_pngfile': url_pngfile})
    except Exception, e:
        print Exception, ":", e
        result = {'Error': 'Something wrong, sorry :-('}
    return jsonify(result)


@app.errorhandler(400)
def badrequest(e):
    return jsonify({'Error': '400 Bad Request. CSRF token missing or incorrect.'}), 400


if __name__ == '__main__':
    app.run(debug=True)
