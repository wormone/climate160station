#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from stationSeries_corr_climateIndex import *


if __name__ == '__main__':
    kw = dict(
        ele = 'r',
        period = 'SO',
        from_last_year_month = 7,
        to_this_year_month = 1,
        from_year = 1952,
        to_year = 2016,
        grade_method = 'fixed')
    # fcst_160_stations_parallel(clf='forest', **kw)
    sites = ['汉中', '安康', '郧县']
    summary, fcsted = [list(range(len(sites))) for _ in range(2)]
    for _ in range(len(sites)):
        summary[_], fcsted[_] = once(sites[_], clf='forest', **kw)
    summary, fcsted = [pd.concat(_) for _ in [summary, fcsted]]
    summary.to_csv('hangjiang_summary.csv', encoding='utf8')
    fcsted.to_csv('hangjiang_fcsted.csv', encoding='utf8')
