#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2016-11-02 16:08:33
# @Author  : wormone (renyuuu@gmail.com)

from __future__ import unicode_literals

from stationSeries_corr_climateIndex import *


if __name__ == '__main__':
    # validation, fcsted, divider, norm = fcst_one_site(
    #     ele='t', period='DJF', site='沈阳',
    #     from_last_year_month = 12, to_this_year_month = 9,
    #     from_year = 1985, to_year = 2015,
    #     grade_method='fixed_temperature', isAbnorm=True,
    #     clf='extratree', nValid=-5, sig=0.001)

    kw = dict(
        ele = 't',
        period = 'JJA',
        from_last_year_month = 7,
        to_this_year_month = 2,
        from_year = 1976,
        to_year = 2016,
        grade_method = 'fixed_temperature',
        isAbnorm = True,
        nValid = -5,
        sig = 0.005)
    fcst_160_stations_parallel(clf='forest', **kw)
    # fcst_one_site(site='乌苏', clf='forest', **kw)
    # once('郴县', clf='forest', **kw)
